import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MysubscriptionsPage } from './mysubscriptions';

@NgModule({
  declarations: [
    MysubscriptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(MysubscriptionsPage),
  ],
})
export class MysubscriptionsPageModule {}
