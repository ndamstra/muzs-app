import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionformPage } from './questionform';

@NgModule({
  declarations: [
    QuestionformPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionformPage),
  ],
})
export class QuestionformPageModule {}
