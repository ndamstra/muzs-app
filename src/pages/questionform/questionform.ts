import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionform',
  templateUrl: 'questionform.html',
})
export class QuestionformPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
