import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { NotificationsPage } from '../notifications/notifications';
import { ConditionsAndPrivacyPage } from '../conditions-and-privacy/conditions-and-privacy';
import { HelpAndFeedbackPage } from '../help-and-feedback/help-and-feedback';
import { FeedbackformPage } from '../feedbackform/feedbackform';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goNotifications(){
  	this.navCtrl.push(NotificationsPage);
  }

  goConditions() {
  	this.navCtrl.push(ConditionsAndPrivacyPage);
  }

  goHelppage() {
  	this.navCtrl.push(HelpAndFeedbackPage);
  }

  goFeedbackform() {
  	this.navCtrl.push(FeedbackformPage);
  }

}
