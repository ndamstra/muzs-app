import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaidPage } from './paid';

@NgModule({
  declarations: [
    PaidPage,
  ],
  imports: [
    IonicPageModule.forChild(PaidPage),
  ],
})
export class PaidPageModule {}
