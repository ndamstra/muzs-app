import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular'

import { MediaPage } from '../media/media';
import { PaidPage } from '../paid/paid';
import { HomePage } from '../home/home';

import { AccountPage } from '../account/account';
import { MychannelPage } from '../mychannel/mychannel';
import { SubscribePage } from '../subscribe/subscribe';
import { SettingsPage } from '../settings/settings';
import { ConditionsAndPrivacyPage } from '../conditions-and-privacy/conditions-and-privacy';
import { HelpAndFeedbackPage } from '../help-and-feedback/help-and-feedback';
import { InformationPage } from '../information/information';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = MediaPage;
  tab3Root = PaidPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goAccount() {
    this.navCtrl.push(AccountPage);
  }

  myChannel(){
  	this.navCtrl.push(MychannelPage);
  }

  goSubscribe(){
  	this.navCtrl.push(SubscribePage);
  }

  goSettings(){
  	this.navCtrl.push(SettingsPage);
  }

  goConditions(){
  	this.navCtrl.push(ConditionsAndPrivacyPage);
  }

  helpAndFeedback(){
  	this.navCtrl.push(HelpAndFeedbackPage);
  }

  goInfo(){
    this.navCtrl.push(InformationPage);
  }

}
