import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConditionsAndPrivacyPage } from './conditions-and-privacy';

@NgModule({
  declarations: [
    ConditionsAndPrivacyPage,
  ],
  imports: [
    IonicPageModule.forChild(ConditionsAndPrivacyPage),
  ],
})
export class ConditionsAndPrivacyPageModule {}
