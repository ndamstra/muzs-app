import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConditionsAndPrivacyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-conditions-and-privacy',
  templateUrl: 'conditions-and-privacy.html',
})
export class ConditionsAndPrivacyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
