import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

// import { StreamingMedia } from '@ionic-native/streaming-media';

/**
 * Generated class for the MediaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-media',
  templateUrl: 'media.html',
})
export class MediaPage {

	content: string = "id";

	public categories: any;
  	constructor(public navCtrl: NavController, public http: HttpClient, private streamingMedia: StreamingMedia) {
      this.getCategories();
  		this.getData();
  	}

  getCategories(){
	const httpOptions = {
		headers: new HttpHeaders({
		'Content-Type':  'application/json',
		'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU0ZjFkOWZiMmU3MmQxOThmNWFjMjhkMjk5MmY5ZGNhYjUwMjhkZmI0M2ZjOGZhZDJkNTQ5MGE2ZDlhN2ZmYzNkYTc0NDlmMTMxOGM0MWE5In0.eyJhdWQiOiIxIiwianRpIjoiZTRmMWQ5ZmIyZTcyZDE5OGY1YWMyOGQyOTkyZjlkY2FiNTAyOGRmYjQzZmM4ZmFkMmQ1NDkwYTZkOWE3ZmZjM2RhNzQ0OWYxMzE4YzQxYTkiLCJpYXQiOjE1NDI3MTIxMzAsIm5iZiI6MTU0MjcxMjEzMCwiZXhwIjoxNTc0MjQ4MTMwLCJzdWIiOiIiLCJzY29wZXMiOltdfQ.X1m5uxCe6ZtAnHQl-uRWK5xb4A5ONw3r82j_gyZzdAi4GcQTYe06GcBdRTmTQnKjYrXc-RB6WOBLOTlQEmHeq4XAtsE_0SV40qk9J15azT-okTrhtjNvQzzzops6fLJlTRyEJh28qd0Ul7vgFJrMn934R6GYA4YxniJckjCxcoTEl_pzdlcxeMGDbJr1blh77TL2kUqMckOr0bbaLOvvYoORasj30rJsAG1vSVOsfgQ5YOO5cMMJBrWA1qd117Tp9h-OYWvAk9FenUdYzARnLGLi8_T5QXrQtMlINFklWZe3b8jH9MrZzlQdnDkQSVDu_1JNs7wBF_PuqoJr15sHsGPHQnIOal034NFi-NVrwNN2NpS5rDsEjpi58BmxuxvQEaDd4B1RwgasahOSLDDCSF0LBquXpzviw-t9zvjM-ukV-mUdDncOHLqqMu8xAYRd-HO_66qYKfpgWPlRLVyESTV0EbBVTQpYY8HNAeGf8oFxv6YE4qc9h8kXxmft8uptgLbnNZ2q39ClStR2N4uNWjjSOAkV0RKEYmPBwC12LT2PvmWDU1VzK843x7-HMRnRBRczAxe7IJ0LMEvCmntD4gejw-l5iChJFqEE8iMhPOcFGvfyMhP9AE0X-Qw1pMe_DXFvZq0v2QBP7uYZeD32b1gWsIPMpwqiTBeMZLNAD1k'
		})
	};

  	let url = 'https://chester.muzs.staging.ilionxinteractivemarketing.nl/api/categories';
  	let data: Observable<any> = this.http.get(url, httpOptions);
  	data.subscribe(result => {
  		this.categories = result;
  	})
  }

  getData(){
  const httpOptions = {
    headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImY5ZTMwNTcxZTY4ZjlkZDhmOGNmODBkNTZmNTEzYTJjNjkxMTJmNDY4OGI5OWZlZTMwZmY3NjBjYzQxY2I3MjYyZjFkNmY4MzQ2NGJkOTQwIn0.eyJhdWQiOiIyIiwianRpIjoiZjllMzA1NzFlNjhmOWRkOGY4Y2Y4MGQ1NmY1MTNhMmM2OTExMmY0Njg4Yjk5ZmVlMzBmZjc2MGNjNDFjYjcyNjJmMWQ2ZjgzNDY0YmQ5NDAiLCJpYXQiOjE1NDI2MzM4MTAsIm5iZiI6MTU0MjYzMzgxMCwiZXhwIjoxNTc0MTY5ODEwLCJzdWIiOiIiLCJzY29wZXMiOltdfQ.GlbZjm20VVXnNxvvcSLfcqv40zMxU0ahwPHdCGLeE5iq16B_FmirP51vtvIC2ivRm54C9Wjzu4uvGbs4m1hiwqGhuDuDFghG-EQMoSQLHUzRuwF_EurOzZqcFLek63p_fbqeU4piS_W7zfe_OiFYzRTg0YYKL_qatNAsiDl5i0GddbiRNNkS_AL4VNV_ZuSJNrM9DbAFUFdHkclnznHrKbgUuy9B04AHYHWkRlIvEXA0NGRJhqRkBwv0nSZT4o4vG_lMgKDrWY8dr0SWb8RACf0lFcRmbxv9orojBQagH8WXIUXtArGq4ixMdr6sgzu5ixQvaky2MR7rBUZfLanC9D6Jf7jtlPH5eW2eFgaisRCwZmPDUuEeYMthlFd65njM8KnY3zIXlrzQhV1hJ5Qdvuv4pCN6zgivUeQpanRgis28ZL8fSDc1tFmZsoXaVOguqjkRa87U4-Jg4B0gsXi7ekvKv_d8E3RtWKcBf9Yv4v1ePE7PPyvNHzKkbx45nD5N8NCxTQX3oE0Nrzz-pg9AAH5Jxr25BHqGLtW5REnp4XhNC9NXgVcdwa3jSnvx0wF3JVLLUlVExqkEmm7mogHRO18AL2OysLlfcfdCo4yiEyvMFHqtX01wGRTQM4jrQ5lEiW4PVBiCzmvf2fHfV-d45GpGB7C5KvBq0Og8YvNieRM'
    })
  };

    let url = 'https://chester.muzs.staging.ilionxinteractivemarketing.nl/api/videos';
    let data: Observable<any> = this.http.get(url, httpOptions);
    data.subscribe(result => {
      this.videos = result;
    })
  }

  startVideo() {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log()},
      errorCallback: () => { console.log()},
      orientation: 'portrait'
    }

    this.streamingMedia.playVideo('https://chester.muzs.staging.ilionxinteractivemarketing.nl/storage/videos/XAos5mm2Z70ja0oI4yWGjzY0N3JKdlyJAV6dqeJm.mp4', options)
  }
}
