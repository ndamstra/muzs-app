import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FaqPage } from '../faq/faq';

import { FeedbackformPage } from '../feedbackform/feedbackform';
import { QuestionformPage } from '../questionform/questionform';

/**
 * Generated class for the HelpAndFeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help-and-feedback',
  templateUrl: 'help-and-feedback.html',
})
export class HelpAndFeedbackPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goFaq() {
  	this.navCtrl.push(FaqPage);
  }

  goQuestionform() {
    this.navCtrl.push(QuestionformPage);
  }

  goFeedbackform() {
    this.navCtrl.push(FeedbackformPage);
  }

}
