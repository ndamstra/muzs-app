import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FeedbackformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedbackform',
  templateUrl: 'feedbackform.html',
})
export class FeedbackformPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
