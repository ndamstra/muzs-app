import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackformPage } from './feedbackform';

@NgModule({
  declarations: [
    FeedbackformPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackformPage),
  ],
})
export class FeedbackformPageModule {}
