import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MychannelPage } from './mychannel';

@NgModule({
  declarations: [
    MychannelPage,
  ],
  imports: [
    IonicPageModule.forChild(MychannelPage),
  ],
})
export class MychannelPageModule {}
