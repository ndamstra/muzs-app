import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AccountPage } from '../account/account';

/**
 * Generated class for the MychannelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mychannel',
  templateUrl: 'mychannel.html',
})
export class MychannelPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goAccount(){
  	this.navCtrl.push(AccountPage);
  }

}
