import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AccordionComponent } from '../components/accordion/accordion';

import { MediaPage } from '../pages/media/media';
import { ContactPage } from '../pages/contact/contact';
import { PaidPage } from '../pages/paid/paid';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { AccountPage } from '../pages/account/account';
import { MychannelPage } from '../pages/mychannel/mychannel';
import { MysubscriptionsPage } from '../pages/mysubscriptions/mysubscriptions';
import { SubscribePage } from '../pages/subscribe/subscribe';
import { SettingsPage } from '../pages/settings/settings';
import { ConditionsAndPrivacyPage } from '../pages/conditions-and-privacy/conditions-and-privacy';
import { HelpAndFeedbackPage } from '../pages/help-and-feedback/help-and-feedback';
import { InformationPage } from '../pages/information/information';

import { FaqPage } from '../pages/faq/faq';
import { QuestionformPage } from '../pages/questionform/questionform';
import { FeedbackformPage } from '../pages/feedbackform/feedbackform';
import { NotificationsPage } from '../pages/notifications/notifications';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { StreamingMedia } from '@ionic-native/streaming-media';

@NgModule({
  declarations: [
    MyApp,
    AccordionComponent,
    MediaPage,
    ContactPage,
    HomePage,
    TabsPage,
    AccountPage,
    MychannelPage,
    MysubscriptionsPage,
    SettingsPage,
    ConditionsAndPrivacyPage,
    HelpAndFeedbackPage,
    InformationPage, 
    PaidPage,
    FaqPage,
    QuestionformPage,
    FeedbackformPage,
    NotificationsPage,
    SubscribePage,
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],

  bootstrap: [
  IonicApp
  ],
  
  entryComponents: [
    MyApp,
    AccordionComponent,
    MediaPage,
    ContactPage,
    HomePage,
    TabsPage,
    AccountPage,
    MychannelPage,
    MysubscriptionsPage,
    SettingsPage,
    ConditionsAndPrivacyPage,
    HelpAndFeedbackPage,
    InformationPage, 
    PaidPage,
    FaqPage,
    QuestionformPage,
    FeedbackformPage,
    NotificationsPage,
    SubscribePage,
  ],

  providers: [
    StatusBar,
    SplashScreen,
    StreamingMedia,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
